This directory contains SMTLIB2.6 files based on the Z3 version of the
ProbeRequestFrame model that we developed. These are not fully
SMTLIB2.6 compatible insofar as they make use of Z3's sequence
extensions.

Note that the Z3 CLI chooses a default solver with different settings
than the one used by our Z3 interface in the "experiments" folder of
this repository. In particular, the Z3 CLI uses a solver based on the
"default" tactic, which performs simplification, logic detection, and
then calls the "smt" tactic. Our Z3 interface uses a solver that only
calls the "smt" tactic.
