(include-book "interface/acl2s-utils/top")
;; we use the ACL2s model for validation
(include-book "wifi-model/wifi-model")
:q
(declaim (optimize (safety 3) (speed 0) (space 0) (debug 3)))
(load "try-load-quicklisp.lsp")
(pushnew (truename "./lisp-z3/") ql:*local-project-directories*)
(ql:register-local-projects)
(ql:quickload :lisp-z3)
(ql:quickload :trivia)

(defpackage :z3-wifi-model
  (:use :cl :z3))

(in-package :z3-wifi-model)

(solver-init)

(register-enum-sort :supported-rates
                    (2 3 4 5 6 9 11 12 18 22 24 36 44 48 54 66 72 96 108 130 131 132 133 134 137 139 140 146 150
                       152 154 164 172 176 182 194 200 224 236))

(defvar *ELEMENTS* nil)
(defvar *REGISTER-TUPLE-SORTS* nil)

(defmacro define-element (name id-val fields constraints &optional length)
  (push `(register-tuple-sort ,name
                              ((elementid . :int)
                               ,@fields))
        *REGISTER-TUPLE-SORTS*)
  `(progn (register-tuple-sort ,name
                               ((elementid . :int)
                                ,@fields))
          (defun ,(intern (concatenate 'string "MAKE-ELEMENT-" (symbol-name name))) (varname)
            (list
             (list varname ,name)
             (list 'and (list '= (list 'tuple-get ,name 'elementid varname) ,id-val)
                   (,constraints varname))))
          (defun ,(intern (concatenate 'string "MAKE-ELEMENT-LENGTH-" (symbol-name name))) (varname)
            ,@(if length
                 (list (list length 'varname))
               '((declare (ignore varname)) 0)))
          (unless (member ,name *ELEMENTS*)
            (setq *ELEMENTS* (cons ,name *ELEMENTS*)))
          ,name))

(defun tuple-seq-length-range (tuple-sort field-name val min max)
  `(and (>= (seq-length (tuple-get ,tuple-sort ,field-name ,val)) ,min)
        (<= (seq-length (tuple-get ,tuple-sort ,field-name ,val)) ,max)))

(define-element :SsidElt 0
  ((body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :SsidElt 'body val 0 32)))

(define-element :SupportedRatesElt 1
  ((body . (:seq :supported-rates)))
  (lambda (val)
    (tuple-seq-length-range :SupportedRatesElt 'body val 1 8)))

(defun sorted-byte-seq-constraints (val lenvar min max)
  (cons 'and
        (append `((= (seq-length ,val) ,lenvar)
                  (>= ,lenvar ,min)
                  (<= ,lenvar ,max))
                (append
                 ;; the first min elements (which are guaranteed to exist)
                 ;; should be sorted
                 (loop for i from 0 below (1- min)
                       collect
                       `(bvult (seq-nth ,val ,i)
                               (seq-nth ,val ,(1+ i))))
                 ;; Any later elements should be sorted if the
                 ;; sequence contains them.
                 (loop for i from min below (1- max)
                       collect
                       `(or (<= ,lenvar ,(1+ i))
                            (bvult (seq-nth ,val ,i)
                                   (seq-nth ,val ,(1+ i)))))))))
;; (sorted-byte-seq-constraints 'x 'x-len 0 4)
;; (sorted-byte-seq-constraints 'x 'x-len 2 5)

(define-element :RequestElt 10
  ((length . :int)
   (body . (:seq (:bv 8))))
  (lambda (val)
    (sorted-byte-seq-constraints `(tuple-get :RequestElt body ,val)
                                 `(tuple-get :RequestElt length ,val)
                                 0 255))
  (lambda (val)
    `(+ 2 (tuple-get :RequestElt length ,val))))

#|
(define-element :RequestElt 10
  ((length . :int)
   (body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :RequestElt 'body val 0 255))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :RequestElt body ,val)))))
|#

(define-element :ExtendedSupportedRates 50
  ((body . (:seq :supported-rates)))
  (lambda (val)
    (tuple-seq-length-range :ExtendedSupportedRates 'body val 1 255))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :ExtendedSupportedRates body ,val)))))

(define-element :DSSSParameterSet 3
  ((body . :int))
  (lambda (val)
    `(and (>= (tuple-get :DSSSParameterSet body ,val) 1)
          (<= (tuple-get :DSSSParameterSet body ,val) 14)))
  (lambda (val) (declare (ignore val)) 3))

(define-element :HTCapabilities 45
  ((body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :HTCapabilities 'body val 26 26))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :HTCapabilities body ,val)))))

(define-element :ExtendedCapabilities 127
  ((body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :ExtendedCapabilities 'body val 1 10))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :ExtendedCapabilities body ,val)))))

(define-element :Interworking 107
  ((body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :Interworking 'body val 1 9))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :Interworking body ,val)))))

(define-element :MeshID 114
  ((body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :MeshID 'body val 0 32))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :MeshID body ,val)))))

(define-element :DMGCapabilities 148
  ((body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :DMGCapabilities 'body val 22 22))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :DMGCapabilities body ,val)))))

(define-element :VHTCapabilities 26
  ((body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :VHTCapabilities 'body val 12 12))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :VHTCapabilities body ,val)))))

(define-element :EstimatedServiceParameters 255
  ((ElementIDExt . :int)
   (Body         . (:seq (:bv 8))))
  (lambda (val)
    `(and (= (tuple-get :EstimatedServiceParameters ElementIDExt ,val) 11)
          (or (= (seq-length (tuple-get :EstimatedServiceParameters body ,val)) 3)
              (= (seq-length (tuple-get :EstimatedServiceParameters body ,val)) 6)
              (= (seq-length (tuple-get :EstimatedServiceParameters body ,val)) 9)
              (= (seq-length (tuple-get :EstimatedServiceParameters body ,val)) 12))))
  (lambda (val)
    `(+ 3 (seq-length (tuple-get :EstimatedServiceParameters body ,val)))))

(define-element :ExtendedRequest 255
  ((ElementIDExt . :int)
   (Body         . (:seq (:bv 8))))
  (lambda (val)
    `(and (= (tuple-get :ExtendedRequest ElementIDExt ,val) 10)
          ,(tuple-seq-length-range :ExtendedRequest 'body val 0 255)))
  (lambda (val)
    `(+ 3 (seq-length (tuple-get :ExtendedRequest body ,val)))))

(define-element :SupportedOperatingClasses 59
  ((Body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :SupportedOperatingClasses 'body val 1 255))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :SupportedOperatingClasses body ,val)))))

(define-element :BSS2040Coexistence 72
  ((Body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :BSS2040Coexistence 'body val 1 255))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :BSS2040Coexistence body ,val)))))

(define-element :SSIDList 84
  ((Body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :SSIDList 'body val 0 255))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :SSIDList body ,val)))))

(define-element :ChannelUsage 97
  ((Body . (:seq (:bv 8))))
  (lambda (val) ;; odd length from 1 to 255
    (cons 'or (loop for i from 1 to 255 by 2
                    collect `(= (seq-length (tuple-get :ChannelUsage body ,val)) ,i))))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :ChannelUsage body ,val)))))

(define-element :MultiBand 158
  ((Body . (:seq (:bv 8))))
  (lambda (val)
    ;; even length from 22 to 255
    (cons 'or (loop for i from 22 to 255 by 2
                    collect `(= (seq-length (tuple-get :MultiBand Body ,val)) ,i))))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :MultiBand body ,val)))))

(define-element :MultipleMacSublayers 170
    ((Body . (:seq (:bv 8))))
  (lambda (val)
    ;; length mod 6 = 1 and between 7 and 255
    (cons 'or (loop for i from 7 to 255 by 6
                    collect `(= (seq-length (tuple-get :MultipleMacSublayers Body ,val)) ,i))))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :MultipleMacSublayers body ,val)))))

(define-element :VenderSpecific 221
  ((Body . (:seq (:bv 8))))
  (lambda (val)
    (tuple-seq-length-range :VenderSpecific 'body val 3 255))
  (lambda (val)
    `(+ 2 (seq-length (tuple-get :VenderSpecific body ,val)))))

(defmacro define-ProbeRequestFrameBody (elements)
  (let ((elements (eval elements)))
  `(progn
     (register-tuple-sort
      :ManagementProbeRequestFrameBody
      ,(loop for elt in elements
             collect `(,elt . ,elt)))
     (defun make-proberequestframebody-query (varname)
       (list `(,varname :ManagementProbeRequestFrameBody)
             (cons 'and
                   ,(cons 'list (loop for elt in elements
                                      collect `(second (,(intern (concatenate 'string "MAKE-ELEMENT-" (symbol-name elt))) `(tuple-get :ManagementProbeRequestFrameBody ,',elt ,varname))))))))
     (defun make-proberequestframebody-size (varname)
       (append '(+ 32)
               ,(cons 'list
                      (loop for elt in elements
                            collect `(,(intern (concatenate 'string "MAKE-ELEMENT-LENGTH-" (symbol-name elt))) `(tuple-get :ManagementProbeRequestFrameBody ,',elt ,varname)))))))))

(define-ProbeRequestFrameBody *ELEMENTS*)
;; This only specifies how the Lisp-Z3 interface will show strings, it
;; does not change any Z3 settings.
(setq z3::*STRING-REP* :list)
(set-params ((timeout 20000)))

(defun get-frame-with-size (size)
  (solver-push)
  (unwind-protect
       (let ((query (make-proberequestframebody-query 'x)))
         (z3-assert-fn (car query) (second query))
         (z3-assert-fn '(x :ManagementProbeRequestFrameBody)
                       (list '= (MAKE-PROBEREQUESTFRAMEBODY-SIZE 'x) size))
         (check-sat))
    (solver-pop)))

(defun solver-reset-and-redefine ()
  (solver-init)
  (register-enum-sort :supported-rates
                    (2 3 4 5 6 9 11 12 18 22 24 36 44 48 54 66 72 96 108 130 131 132 133 134 137 139 140 146 150
                       152 154 164 172 176 182 194 200 224 236))
  (loop for rts in *REGISTER-TUPLE-SORTS*
        do (eval rts))
  (set-params ((timeout 20000)))
  (define-ProbeRequestFrameBody *ELEMENTS*))

(ql:quickload :cl-json)
(defun acl2::main (argv)
  (if (not (equal (length argv) 6))
      (format t "Usage: ~a <num-trials> <start> <end> <resolution> <output>" (car argv))
    (let ((n-trials (parse-integer (second argv)))
          (start-size (parse-integer (third argv)))
          (end-size (parse-integer (fourth argv)))
          (resolution (parse-integer (fifth argv)))
          (output-file (sixth argv)))
      (solver-reset-and-redefine)
      (handler-case
          (let ((data
                 (loop for i from start-size below end-size by resolution
                       do (print i)
                       append (loop for j below n-trials
                                    collect (let* ((start-time (get-internal-real-time))
                                                   (res (get-frame-with-size i))
                                                   (end-time (get-internal-real-time)))
                                              (list i (if (symbolp res) res :sat) (- end-time start-time)))))))
            (with-open-file (stream output-file :if-exists :supersede :direction :output :if-does-not-exist :create)
              (json:use-explicit-encoder)
              (json:encode-json
               (cons :list (loop for (size res time) in data
                                 collect `(:object
                                           (:kind . "z3")
                                           (:size . ,size)
                                           (:res . ,res)
                                           (:time . ,(/ time internal-time-units-per-second)))))
               stream)))
        (error (e) (format t "Error: ~a" e)
               (sb-ext:exit :code 1)))
      (sb-ext:exit :code 0))))


(acl2s::save-exec "z3-only" nil
                  :init-forms '((acl2::value :q))
                  :toplevel-args "--eval '(acl2::main sb-ext:*posix-argv*)' --disable-debugger")
