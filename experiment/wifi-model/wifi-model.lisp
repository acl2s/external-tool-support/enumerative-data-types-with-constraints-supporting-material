#|

Author: Pete Manolios
Date:   3/6/2019

This is derived from the Lustre model. Some comments are included
below that compare this model with the Lustre model.

The key idea here is that the model we have here is defined almost
exclusively using defdata. That means that generating tests becomes
more of a computational problem than a satisfiability problem. This
leads to significant performance improvements over the use of SMT
solvers.

|#

(in-package "ACL2S")
(include-book "acl2s/sorting/sorting" :dir :system :ttags :all)
(include-book "defdata-helpers")
(include-book "deflist")

(acl2s-defaults :set sampling-method :uniform-random)
(acl2s-defaults :set num-witnesses 0)
(acl2s-defaults :set num-counterexamples 1)
(acl2s-defaults :set num-print-witnesses 0)
(acl2s-defaults :set num-print-counterexamples 1)

#|
**********************************************************
Compare uint2, ..., uint48. The ACL2s definitions are what
we want. In the Lustre model, all of these types are just
ints, which requires the use of more properties.

If we remove all of these range formulas from the RadioTap.lus
file, we wind up with the following stats:

        Before      After
lines     4268       1401
size      244K        65K

For example, 

node RequestElementTypeAssertions is not needed.
**********************************************************
|#

(defnatrange uint2  (expt 2 2))
(defnatrange uint3  (expt 2 3))
(defnatrange uint4  (expt 2 4))
(defnatrange uint5  (expt 2 5))
(defnatrange uint8  (expt 2 8))  ; bytep is a common lisp name, so i'm using uint8
(defnatrange uint12 (expt 2 12))
(defnatrange uint28 (expt 2 28))
(defnatrange uint32 (expt 2 32))
(defnatrange uint48 (expt 2 48))

#|
**********************************************************
Notice that we have subtypes, so we can check the following.
This just adds confidence in the model.
Notice that the following require proof.
**********************************************************
|#

;; Sanity checks:
(defdata-subtype uint2 uint3)
(defdata-subtype uint3 uint4)
(defdata-subtype uint4 uint5)
(defdata-subtype uint5 uint8)
(defdata-subtype uint8 uint12)
(defdata-subtype uint12 uint28)
(defdata-subtype uint28 uint32)
(defdata-subtype uint32 uint48)

#|
**********************************************************
We can also use property-based testing to make claims that
counterexamples exist.
This also increases our confidence in the model and prevents
later updates from breaking things.
**********************************************************
|#

(must-fail
 (test? (implies (uint12p n) (uint8p n))))

(defdata exact0 0)
(defdata exact1 1)
(defdata exact3 3)
(defdata exact10 10)
(defdata exact11 11)
(defdata exact26 26)
(defdata exact45 45)
(defdata exact50 50)
(defdata exact59 59)
(defdata exact72 72)
(defdata exact84 84)
(defdata exact97 97)
(defdata exact107 107)
(defdata exact114 114)
(defdata exact127 127)
(defdata exact148 148)
(defdata exact158 158)
(defdata exact170 170)
(defdata exact221 221)
(defdata exact255 255)

#|
**********************************************************
Below are examples of where the spec requires that certain
fields have values in a range.
**********************************************************
|#

(defdata validChannel (range integer (1 <= _ <= 14)))
(defdata range0-32 (range integer (0 <= _ <= 32)))

(defdata byte-array (listof uint8))

(defdata-list byte8   uint8 8 8)
(defdata-list byte9   uint8 1 9)
(defdata-list byte10  uint8 1 10)
(defdata-list byte12  uint8 12 12)
(defdata-list byte22  uint8 22 22)
(defdata-list byte26  uint8 26 26)
(defdata-list byte32  uint8 0 32)
(defdata-list byte255 uint8 0 255)
(defdata-list byte1-255 uint8 1 255)
(defdata-list byte3-255 uint8 3 255)

(defdata Dot11FrameType uint2)

(defdata Dot11FrameTypeEnumType
  (record (MGMT_FRAME      . Dot11FrameType)
          (CONTROL_FRAME   . Dot11FrameType)
          (DATA_FRAME      . Dot11FrameType)
          (EXTENSION_FRAME . Dot11FrameType)))

(defconst *Dot11FrameTypeEnum*
  (Dot11FrameTypeEnumType 0 1 2 3))

(defdata Dot11MgmtSubTypeType uint3)

(defdata Dot11MgmtSubTypeEnumType
  (record (MGT_ASSOC_REQ     . Dot11MgmtSubTypeType)
          (MGT_ASSOC_RESP    . Dot11MgmtSubTypeType)
          (MGT_REASSOC_REQ   . Dot11MgmtSubTypeType)
          (MGT_REASSOC_RESP  . Dot11MgmtSubTypeType)
          (MGT_PROBE_REQ     . Dot11MgmtSubTypeType)
          (MGT_PROBE_RESP    . Dot11MgmtSubTypeType)))

(defconst *Dot11MgmtSubTypeEnum*
  (Dot11MgmtSubTypeEnumType 0 1 2 3 4 5))

(defdata FrameType
  (record 
   (version     . uint2)                ; -- required uint32 version = 1;           //2 bits       - Version
   (ptype       . Dot11FrameType)       ; -- required Dot11FrameType type = 2;      //2 bits       - Frame Type
   (subtype     . Dot11MgmtSubTypeType) ; -- required Dot11MgmtSubType subtype = 3; //4 bits wide  - Frame Subtype
   (toDs        . boolean)              ; -- required bool toDs = 4;                //1 bit wide   - DS status to
   (fromDs      . boolean)              ; -- required bool fromDs = 5;              //1 bit wide   - DS status from
   (moreFrag    . boolean)              ; -- required bool moreFrag = 6;            //1 bit wide   - More fragments? T/F
   (retry       . boolean)              ; -- required bool retry = 7;               //1 bit wide   - This frame is being re-transmitted? T/F
   (powerMgmt   . boolean)              ; -- required bool powerMgmt = 8;           //1 bit wide   - Power management mode? T/F
   (moreData    . boolean)              ; -- required bool moreData = 9;            //1 bit wide   - Is data buffered? T/F
   (WEP         . boolean)              ; -- required bool WEP = 10;                //1 bit wide   - Is data protected? T/F (protected frame)
   (order       . boolean)              ; -- required bool order = 11;              //1 bit wide   - Is the data ordered? T/F
   (durationID  . uint12)               ; -- required uint32 durationID = 12;       //16 bits wide - Duration 
   (addr1       . uint48)               ; -- required uint64 addr1 = 13;            //48 bits wide - MAC addr of AP (destination)
   (addr2       . uint48)               ; -- required uint64 addr2 = 14;            //48 bits wide - MAC addr of transmitter
   (addr3       . uint48)               ; -- required uint64 addr3 = 15;            //48 bits wide - MAC addr of final destination (AP)
   (fragmentNum . uint4)                ; -- required uint32 fragmentNum = 16;      //4 bits wide  - Fragment Number
   (seqNumber   . uint12)               ; -- required uint32 seqNumber = 17;        //12 bits wide - Sequence number
   (addr4       . uint48)               ; -- required uint64 addr4 = 18;            //48 bits wide - 
   (checksum    . uint32)               ; -- required fixed32 checksum = 20;        //32 bits wide - 
   ))

#|
**********************************************************
Notice that FrameTypeAssertions node is redundant since its
requirements are encoded in the above data definition.

Since the Lustre file uses constants, one has to carefully check all
the fields are mentioned and that the bounds are correct.
**********************************************************
|#

(defdata SSIDElementType
  (record
   (ElementID . exact0)
   (Body      . byte32)))
;;; SSIDElementTypeAssertions node is redundant (encoded in the ACL2s type)
;;; ValidSSIDElementConstraints node is redundant (encoded in the ACL2s type)

#|
**********************************************************
The Lustre model works as follows. The Body is an array of size 32 and
the length is a byte.  Constraints later on require that the length is
between 0 and 32. Suppose the length is 10, then elements 11-32 are
ignored, so the length tells us how big the array is and the rest of
the data is essentially garbage.

In our model, we have an array whose size is between 0 and 32, as per
the spec, so we don't need a length field. We can compute the length.

This same pattern is used throughout. 
**********************************************************

|#

(defconst *supported-rates*
  '(2 3 4 5 6 9 11 12 18 22 24 36 44 48 54 66 72 96 108 130 131 132 133 134 137 139 140 146 150 
    152 154 164 172 176 182 194 200 224 236))

(defdata supportedRatesType
  (enum *supported-rates*))
;;; validSupportedRate node is redundant (encoded in the ACL2s type)

(defdata-list supportedRate8 supportedRatesType 1 8)

(defdata SupportedRatesElementType
  (record
   (ElementID . exact1)
   (Body      . supportedRate8)))
;;; SupportedRatesElementTypeAssertions node is redundant (encoded in the ACL2s type)
;;; ValidSupportedRatesElementConstraints node is redundant (encoded in the ACL2s type)

#|
**********************************************************
We use exact1, a singleton type, to make it clear that
the ElementID has to be 1.

The Lustre model enforces this via extra constraints.
**********************************************************
|#

(defdata-ordered-list byte255-increasing uint8 0 255) 

#|
**********************************************************
The above shows three interesting things.

1. We can define a function that checks if an arbtirary-length array
   is increasing. Compare this with
   RequestElementOrderedElementIDConstraint, which requires n
   constraints, where n is the size of the array.

2. This provides an example of a custom data-type. Once we define a
   custom data type we can use it to build new types.

3. It would be nice to provide support for this directly 
   with defdata.
**********************************************************
|#

(must-fail
 (test? (implies (byte255p x) (byte255-increasingp x))))

(defdata RequestElementType
  (record
   (ElementID . exact10)
   (Body      . byte255-increasing)))


;;; RequestElementTypeAssertions node is redundant (encoded in the ACL2s type)

;; RequestElementOrderedElementIDConstraint is not needed since
;; it was used to define  ValidRequestElementConstraints

;;; ValidRequestElementIDConstraint node is redundant (encoded in the ACL2s type)
;;; RequestElementValidRequestElementIDConstraint node is redundant (encoded in the ACL2s type)

(defdata-list supportedRate255 supportedRatesType 1 255)

(defdata ExtendedSupportedRatesElementType
  (record
   (ElementID . exact50)
   (Body      . supportedRate255)))
;;; ExtendedSupportedRatesElementTypeAssertions node is redundant (encoded in the ACL2s type)
;;; ValidExtendedSupportedRatesElementConstraints node is redundant (encoded in the ACL2s type)

(defdata DSSSParameterSetElementType
  (record
   (ElementID . exact3)
   (Body      . validChannel)))
; Not sure why spec says a 1-byte array instead of just a byte for channel

;;; DSSSParameterSetElementTypeAssertions node is redundant (encoded in the ACL2s type)
;; ValidDSSSParameterSetElementConstraints node is also redundant

(defdata HTCapabilitiesElementType
  (record
   (ElementID . exact45)
   (Body      . byte26)))
;;; Redundant: HTCapabilitiesElementTypeAssertions, ValidHTCapabilitiesElementConstraints

(defdata ExtendedCapabilitiesElementType
  (record
   (ElementID . exact127)
   (Body      . byte10)))
;;; Redundant: ExtendedCapabilitiesElementTypeAssertions, ValidExtendedCapabilitiesElementConstraints

(defdata-list-rng byte1-9 uint8 (1 3 7 9))

(defdata InterworkingElementType
  (record
   (ElementID . exact107)
   (Body      . byte9)))
;;; Redundant: InterworkingElementTypeAssertions, ValidInterworkingElementConstraints

(defdata MeshIDElementType
  (record
   (ElementID . exact114)
   (Body      . byte32)))
;;; Redundant: MeshIDElementTypeAssertions, ValidMeshIDElementConstraints


(defdata DMGCapabilitiesElementType
  (record
   (ElementID . exact148)
   (Body      . byte22)))
;;; Redundant: DMGCapabilitiesElementTypeAssertions, ValidDMGCapabilitiesElementConstraints

(defdata VHTCapabilitiesElementType
  (record
   (ElementID . exact26)
   (Body      . byte12)))
;;; Redundnant: VHTCapabilitiesElementTypeAssertions, ValidVHTCapabilitiesElementConstraints

(defdata-list-rng byte3-12 uint8 (3 6 9 12))

(defdata EstimatedServiceParametersElementType
  (record
   (ElementID    . exact255)
   (ElementIDExt . exact11)
   (Body         . byte3-12)))
;;; Redundant: EstimatedServiceParametersElementTypeAssertions, ValidEstimatedServiceParametersElementConstraints

(defdata ExtendedRequestElementType
  (record
   (ElementID    . exact255)
   (ElementIDExt . exact10)
   (Body         . byte255)))
;;; Redundant: ExtendedRequestElementTypeAssertions, ValidExtendedRequestElementConstraints


(defdata SupportedOperatingClassesElementType
  (record
   (ElementID . exact59)
   (Body      . byte1-255)))
;;; Redundnant: SupportedOperatingClassesElementTypeAssertions,
;;; ValidSupportedOperatingClassesElementConstraints 

(defdata BSS2040CoexistenceElementType
  (record
   (ElementID . exact72)
   (Body      . byte1-255)))
;;; Redundant: BSS2040CoexistenceElementTypeAssertions, ValidBSS2040CoexistenceElementConstraints

(defdata SSIDListElementType
  (record
   (ElementID . exact84)
   (Body      . byte255)))
;;; Redundant: SSIDListElementTypeAssertions, ValidSSIDListElementConstraints


(defdata-list-rng byte255-odd uint8 (gen-skip 1 255 2))

(defdata ChannelUsageElementType
  (record
   (ElementID . exact97)
   (Body      . byte255-odd)))
;;; Redundant: ChannelUsageElementTypeAssertions, ValidChannelUsageElementConstraints

(defdata-list-rng byte22-255 uint8 (gen-skip 22 254 2))

(defdata MultiBandElementType
  (record
   (ElementID . exact158)
   (Body      . byte22-255)))
;;; Redundant: MultiBandElementTypeAssertions, ValidMultiBandElementConstraints

;;; This element's length must be 1 mod 6 and must also be at least 7
(defdata-list-rng byte255-7-6 uint8 (gen-skip 7 255 6))

(defdata MultipleMacSublayersElementType
  (record
   (ElementID . exact170)
   (Body      . byte255-7-6)))
;;; Redundant: MultipleMacSublayersElementTypeAssertions, ValidMultipleMacSublayersElementConstraints

(defdata VenderSpecificElementType
  (record
   (ElementID . exact221)
   (Body      . byte3-255)))
;;; Redundant: VenderSpecificElementTypeAssertions, ValidVenderSpecificElementConstraints

(defdata ManagementProbeRequestFrameBodyType
  (record
   (SSID                       . SSIDElementType)
   (SupportedRates             . SupportedRatesElementType)
   (DSSSParameterSet           . DSSSParameterSetElementType)
   (Request                    . RequestElementType)
   (HTCapabilities             . HTCapabilitiesElementType)
   (extSupportedRates          . ExtendedSupportedRatesElementType)
   (SupportedOperatingClasses  . SupportedOperatingClassesElementType)
   (BSS2040Coexistence         . BSS2040CoexistenceElementType)
   (SSIDList                   . SSIDListElementType)
   (ChannelUsage               . ChannelUsageElementType)
   (Interworking               . InterworkingElementType)
   (MeshID                     . MeshIDElementType)
   (ExtendedCapabilities       . ExtendedCapabilitiesElementType)
   (DMGCapabilities            . DMGCapabilitiesElementType)
   (MultiBand                  . MultiBandElementType)
   (MultipleMacSublayers       . MultipleMacSublayersElementType)
   (VHTCapabilities            . VHTCapabilitiesElementType)
   (VenderSpecific             . VenderSpecificElementType)
   (ExtendedRequest            . ExtendedRequestElementType)
   (EstimatedServiceParameters . EstimatedServiceParametersElementType)))
;;; Redundant: ManagementProbeRequestFrameBodyTypeAssertions (only calls other ___TypeAssertions nodes, which are themselves redundant)

;;; Leaving out checks for powerMgmt and seqNumber (commented out in Lustre spec)
(definec ValidManagementProbeRequestFrameConstraints (frame :frametype) :bool
  (and (= (FrameType-version frame)    0)
       (= (FrameType-ptype frame)
          (Dot11FrameTypeEnumType-MGMT_FRAME *Dot11FrameTypeEnum*))
       (= (FrameType-subtype frame)
          (Dot11MgmtSubTypeEnumType-MGT_PROBE_REQ *Dot11MgmtSubTypeEnum*))
       (not (FrameType-toDs     frame))
       (not (FrameType-fromDs   frame))
       (not (FrameType-moreFrag frame))
       (not (FrameType-retry    frame))
       (not (FrameType-moreData frame))
       (not (FrameType-WEP      frame))
       (FrameType-order frame)
       (= (FrameType-durationID  frame) 0)
       (= (FrameType-addr1       frame) 0)
       (= (FrameType-addr2       frame) 0)
       (= (FrameType-addr3       frame) 0)
       (= (FrameType-fragmentNum frame) 0)
       (= (FrameType-addr4       frame) 0)
       (= (FrameType-checksum    frame) 0)))

(defdata-size-opt DSSSParameterSetElementType
  DSSSParameterSet-size
  3
  3)

(defdata-size-opt RequestElementType
  Request-size 
  (+ 2 (len (RequestElementType-Body e)))
  257)

(defdata-size-opt HTCapabilitiesElementType
  HTCapabilities-size 
  (+ 2 (len (HTCapabilitiesElementType-Body e)))
  28)

(defdata-size-opt ExtendedSupportedRatesElementType
  ExtendedSupportedRates-size
  (+ 2 (len (ExtendedSupportedRatesElementType-Body e)))
  257)

(defdata-size-opt SupportedOperatingClassesElementType
  SupportedOperatingClasses-size
  (+ 2 (len (SupportedOperatingClassesElementType-Body e)))
  257)

(defdata-size-opt BSS2040CoexistenceElementType
  BSS2040Coexistence-size
  (+ 2 (len (BSS2040CoexistenceElementType-Body e)))
  257)

(defdata-size-opt SSIDListElementType
  SSIDList-size 
  (+ 2 (len (SSIDListElementType-Body e)))
  257)

(defdata-size-opt ChannelUsageElementType
  ChannelUsage-size 
  (+ 2 (len (ChannelUsageElementType-Body e)))
  257)

(defdata-size-opt InterworkingElementType
  Interworking-size 
  (+ 2 (len (InterworkingElementType-Body e)))
  11)

(defdata-size-opt MeshIDElementType
  MeshID-size 
  (+ 2 (len (MeshIDElementType-Body e)))
  34)

(defdata-size-opt ExtendedCapabilitiesElementType
  ExtendedCapabilities-size
  (+ 2 (len (ExtendedCapabilitiesElementType-Body e)))
  12)

(defdata-size-opt DMGCapabilitiesElementType
  DMGCapabilities-size 
  (+ 2 (len (DMGCapabilitiesElementType-Body e)))
  24)

(defdata-size-opt MultiBandElementType
  MultiBand-size 
  (+ 2 (len (MultiBandElementType-Body e)))
  256)

(defdata-size-opt MultipleMacSublayersElementType
  MultipleMacSublayers-size
  (+ 2 (len (MultipleMacSublayersElementType-Body e)))
  255)

(defdata-size-opt VHTCapabilitiesElementType
  VHTCapabilities-size 
  (+ 2 (len (VHTCapabilitiesElementType-Body e)))
  14)

(defdata-size-opt VenderSpecificElementType
  VenderSpecific-size 
  (+ 2 (len (VenderSpecificElementType-Body e)))
  257)

(defdata-size-opt ExtendedRequestElementType
  ExtendedRequest-size 
  (+ 3 (len (ExtendedRequestElementType-Body e)))
  258)

(defdata-size-opt EstimatedServiceParametersElementType
  EstimatedServiceParameters-size
  (+ 3 (len (EstimatedServiceParametersElementType-Body e)))
  15)

(definec-no-test ManagementProbeRequestSize (msg :managementproberequestframebodytype) :nat
  (+ 32
     (DSSSParameterSet-size (ManagementProbeRequestFrameBodyType-DSSSParameterSet msg))
     (Request-size (ManagementProbeRequestFrameBodyType-Request msg))
     (HTCapabilities-size (ManagementProbeRequestFrameBodyType-HTCapabilities msg))
     (ExtendedSupportedRates-size (ManagementProbeRequestFrameBodyType-extSupportedRates msg))
     (SupportedOperatingClasses-size (ManagementProbeRequestFrameBodyType-SupportedOperatingClasses msg))
     (BSS2040Coexistence-size (ManagementProbeRequestFrameBodyType-BSS2040Coexistence msg))
     (SSIDList-size (ManagementProbeRequestFrameBodyType-SSIDList msg))
     (ChannelUsage-size (ManagementProbeRequestFrameBodyType-ChannelUsage msg))
     (Interworking-size (ManagementProbeRequestFrameBodyType-Interworking msg))
     (MeshID-size (ManagementProbeRequestFrameBodyType-MeshID msg))
     (ExtendedCapabilities-size (ManagementProbeRequestFrameBodyType-ExtendedCapabilities msg))
     (DMGCapabilities-size (ManagementProbeRequestFrameBodyType-DMGCapabilities msg))
     (MultiBand-size (ManagementProbeRequestFrameBodyType-MultiBand msg))
     (MultipleMacSublayers-size (ManagementProbeRequestFrameBodyType-MultipleMacSublayers msg))
     (VHTCapabilities-size (ManagementProbeRequestFrameBodyType-VHTCapabilities msg))
     (VenderSpecific-size (ManagementProbeRequestFrameBodyType-VenderSpecific msg))
     (EstimatedServiceParameters-size (ManagementProbeRequestFrameBodyType-EstimatedServiceParameters msg))
     (ExtendedRequest-size (ManagementProbeRequestFrameBodyType-ExtendedRequest msg))))

;; Upper bound on frame size: 2709 for all max-sized optional elements, plus 32 for required elements
(defthm-no-test small-frames
  (implies (ManagementProbeRequestFrameBodyTypep msg)
           (>= 2741 (ManagementProbeRequestSize msg))))
