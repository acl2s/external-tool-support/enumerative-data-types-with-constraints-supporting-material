(in-package "ACL2S")

(defmacro m-symbl (l)
  `(gen-sym-sym ,l name))

(defmacro defdata-opt (name)
  (let* ((oname (m-symbl `(Opt- ,name))))
    `(progn
       (defdata ,oname (oneof ,name nil)))))

(defmacro defdata-size-opt (name szname exp bound)
  (let* ((oname (m-symbl `(Opt- ,name)))
         (namep (m-symbl `(,name p)))
         (onamep (m-symbl `(,oname p)))
         (defname (m-symbl `(,szname -DEFINITION-RULE)))
         (def-thm1 (m-symbl `(,szname -size-1-thm)))
         (def-thm2 (m-symbl `(,szname -size-2-thm)))
         (type-thm1 (m-symbl `(,szname -type-1-thm)))
         (def-linear (m-symbl `(,szname -size-linear))))
    `(progn
       (defdata-opt ,name)
       (definec ,szname (e ,oname) nat
         (if e ,exp 0))
       (defthm ,def-thm1
         (implies (and (,onamep e) e)
                  (equal (,szname e) ,exp)))
       (defthm ,def-thm2
         (implies (null e)
                  (equal (,szname e) 0)))
       (defthm ,def-linear
         (implies (,onamep e)
                  (<= (,szname e) ,bound))
         :rule-classes :linear
         :hints (("goal" :instructions
                  (:bash
                   (when-not-proved
                    (:in-theory (enable ,namep))
                    :bash)))))
       (defthm ,type-thm1
         (implies (,onamep a)
                  (natp (,szname a)))
         :rule-classes (:type-prescription))
       (in-theory (disable ,defname ,def-thm1))
       (add-default-hints! '((stage ,def-thm1)))
       (must-fail (test? (implies (,onamep e)
                                  (<= (,szname e) ,(1- bound))))))))

