(in-package "ACL2S")
(include-book "acl2s/sorting/sorting" :dir :system :ttags :all)
(set-termination-method :ccg)

(defmacro m-symbl (l)
  `(gen-sym-sym ,l name))

(defun deflist-fn (name type min max)
  (declare (xargs :guard 
                  (and (symbolp name) (symbolp type)
                       (natp min) (natp max) (<= min max))))
  (let* ((namep (m-symbl `(,name p)))
         (namedef (m-symbl `(,namep -definition-rule)))
         (min>0? (> min 0))
         (name-core (m-symbl `(,name -core)))
         (name-corep (m-symbl `(,name-core p)))
         (fc-name (m-symbl `(,name -fc-rule)))
         (tlp-name (m-symbl `(,name -is-tlp)))
         (linear-name (m-symbl `(,name -linear)))
         (enum (m-symbl `(nth- ,name-core)))
         (enum-acc (m-symbl `(,enum /acc)))
         (body0 (cond ((= min max)
                       `((equal (len x) ,min)))
                      ((not min>0?)
                       `((<= (len x) ,max)))
                      (t `((>= (len x) ,min)
                           (<= (len x) ,max)))))
         (body `(and (,name-corep x)
                     ,@body0))
         (defdata `(defdata ,name-core (listof ,type)
                     :do-not-alias t
                     :min-rec-depth ,min
                     :max-rec-depth ,max))
         (recognizer `(definec ,namep (x :all) :bool ,body))
         (fc `(defthm ,fc-name
                (implies ,body (,namep x))
                :rule-classes :forward-chaining
                :hints (("goal" :use ,namedef))))
         (tlp-rule `(defthm ,tlp-name
                      (implies (,namep x)
                               (and (true-listp x)
                                    (,name-corep x)))
                      :hints (("goal" :in-theory (enable true-listp)))
                      :rule-classes ((:forward-chaining)
                                     (:compound-recognizer)
                                     (:rewrite :backchain-limit-lst 1))))
         (linear-rule `(defthm ,linear-name
                         (implies (,namep x) (and ,@body0))
                         :rule-classes ((:forward-chaining))))
         (reg `(register-type ,name
                              :predicate ,namep
                              :enumerator ,enum
                              :enum/acc ,enum-acc
                              :recp t
                              :min-rec-depth ,min
                              :max-rec-depth ,max)))
    `(progn ,defdata ,recognizer ,fc ,tlp-rule ,linear-rule ,reg
            (in-theory (disable ,name-corep ,namep)))))

(defmacro defdata-list (name type min max)
  (deflist-fn name type min max))

#|

Here is an example

:trans1 (defdata-list foo integer 10 20)

(defdata-list foo integer 10 20)

(test? (implies (foop x)
                (<= (len x) 19)))

|#

(definec increasingp (l :integer-list) :bool
  (or (endp l)
      (endp (rest l))
      (and (< (first l) (second l))
           (increasingp (rest l)))))

(defun deflist-ordered-fn (name type min max)
  (declare (xargs :guard 
                  (and (symbolp name) (symbolp type)
                       (natp min) (natp max) (<= min max))))
  (let* ((namep (m-symbl `(,name p)))
         (name-ord (m-symbl `(,name -ord)))
         (name-ordp (m-symbl `(,name-ord p)))
         (name-core-ord (m-symbl `(,name-ord -core)))
         (name-core-ordp (m-symbl `(,name-core-ord p)))
         (enum (m-symbl `(nth- ,name -ord)))
         (enum-acc (m-symbl `(,enum /acc)))
         (oenum (m-symbl `(,enum -enum)))
         (oenum-acc (m-symbl `(,oenum /acc)))
         (thm-core-name (m-symbl `(,name-core-ord -is-int-list)))
         (thm-name (m-symbl `(,name-ord -is-int-list)))
         (defdata `(defdata-list ,name-ord ,type ,min ,max))
         (data-core-thm `(defthm ,thm-core-name
                           (implies (,name-core-ordp x)
                                    (integer-listp x))))
         (data-thm `(defthm ,thm-name
                      (implies (,name-ordp x)
                               (integer-listp x))))
         (recognizer `(definec ,namep (x all) bool
                        (and (,name-ordp x)
                             (increasingp x))))
         (enum-def `(defun ,oenum (n)
                      (declare (xargs :mode :program :guard (natp n)))
                      (make-unique-ordered (,enum n) ,min)))
         (enum-acc-def
          `(defun ,oenum-acc (size1 _seed)
             (declare (xargs :mode :program
                             :guard (if (natp size1)
                                        (unsigned-byte-p '31 _seed)
                                      'nil)))
             (declare (ignorable size1 _seed))
             (mv-let (a b) (,enum-acc size1 _seed)
                     (mv (make-unique-ordered a ,min) b))))
         (reg `(register-type ,name
                              :predicate ,namep
                              :enumerator ,oenum
                              :enum/acc ,oenum-acc
                              :recp t
                              :min-rec-depth ,min
                              :max-rec-depth ,max)))
    `(progn ,defdata ,data-core-thm ,data-thm ,recognizer
            ,enum-def ,enum-acc-def ,reg)))

(defmacro defdata-ordered-list (name type min max)
  (deflist-ordered-fn name type min max))

#|

Here is an example

:trans1 (defdata-ordered-list foo integer 10 20)
:trans1 (defdata-ordered-list byte255-increasing uint8 0 255) 

(defdata-ordered-list foo integer 10 20)

(test? (implies (foop x)
                (<= (len x) 19)))

(time$ (nth-byte255-increasing-ord-enum/acc 128 12315123))
(time$ (nth-byte255-increasing-ord-enum/acc 255 12315123))

(time$ (nth-byte255-increasing-ord-enum/acc 10 12315123))

(b* (((mv l &)
      (nth-byte255-increasing-ord-enum/acc 255 12315123)))
  (byte255-increasingp l))

|#

(definec gen-skip (min :int max :int skip :pos) :integer-list
  (if (> min max)
      nil
    (cons min (gen-skip (+ min skip) max skip))))

(definec fix-len (e :integer-list len :int lens :integer-list) :integer-list
  (cond ((endp e) e)
        ((member-equal len lens) e)
        (t (fix-len (cdr e) (1- len) lens))))

(definec lmin (l :integer-list m :int) :int
  (if (endp l)
      m
    (lmin (cdr l) (min m (car l)))))

(definec lmax (l :integer-list m :int) :int
  (if (endp l)
      m
    (lmax (cdr l) (max m (car l)))))

(defun deflist-rng-fn (name type rng)
  (declare (xargs :guard 
                  (and (symbolp name) (symbolp type)
                       (nat-listp rng) (consp rng))))
  (let* ((namep (m-symbl `(,name p)))
         (min-depth (lmin rng (car rng)))
         (max-depth (lmax rng (car rng)))
         (name-rng (m-symbl `(,name -RNG)))
         (name-rngp (m-symbl `(,name -RNGP)))
         (enum-rng (m-symbl `(nth- ,name -rng)))
         (enum-rng-acc (m-symbl `(nth- ,name -rng/acc)))
         (enum-name (m-symbl `(nth- ,name -my-enum)))
         (enum-acc-name (m-symbl `(nth- ,name /my-acc-enum)))
         (defdata `(defdata ,name-rng (listof ,type)
                     :do-not-alias t
                     :min-rec-depth ,min-depth
                     :max-rec-depth ,max-depth))
         (body `(and (,name-rngp x)
                     (member-equal (len x) ',rng)
                     t))
         (recognizer `(definec ,namep (x :all) :bool
                        ,body))
         (enum `(defun ,enum-name (n)
                  (declare (xargs :mode :program :guard (natp n)))
                  (let* ((e (,enum-rng n))
                         (len (len e)))
                    (fix-len e len ',rng))))
         (enum-acc `(defun ,enum-acc-name (size1 _seed)
                      (declare (xargs :mode :program 
                                      :guard (if (natp size1)
                                                 (unsigned-byte-p '31 _seed)
                                               'nil)))
                      (declare (ignorable size1 _seed))
                      (mv-let (a b) (,enum-rng-acc size1 _seed)
                              (mv (fix-len a (len a) ',rng) b))))
         (reg `(register-type ,name
                              :predicate ,namep
                              :enumerator ,enum-name
                              :enum/acc ,enum-acc-name
                              :recp t
                              :min-rec-depth ,min-depth
                              :max-rec-depth ,max-depth)))
    `(progn ,defdata ,recognizer ,enum ,enum-acc ,reg)))

(defmacro defdata-list-rng-aux (name type rng)
  (deflist-rng-fn name type rng))

#|

Here is an example

:trans1 (defdata-list-rng-aux foo integer (10 20))
(defdata-list-rng-aux foo integer (10 20))

(test? (implies (foop x)
                (<= (len x) 19)))

(test? (implies (foop x)
                (/= (len x) 20)))

:trans1 (defdata-list-rng-aux byte1-9 uint8 (1 3 7 9))
|#

; A version of defdata-list-rng that support an explicitly provided
; list of lengths or an expression that evaluates to a list of lengths.

(defmacro defdata-list-rng (name type gen)
  (if (nat-listp gen)
      `(defdata-list-rng-aux ,name ,type ,gen)
    `(make-event
      `(defdata-list-rng-aux ,',name ,',type ,,gen))))

#|

Here are some tests

:trans1 (defdata-list-rng foo nat (1 3 5 7 9))
:trans1 (defdata-list-rng foo nat (1 3 5 7 9 a))
:trans1 (defdata-list-rng foo nat (gen-skip 1 10 2))
:trans (defdata-list-rng foo nat (gen-skip 1 10 2))
:trans1 (defdata-list-rng foo nat (gen-skip 1 10 2 x))
:trans1 (defdata-list-rng foo nat (gen 1 10 2 x))

|#

