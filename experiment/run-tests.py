#!/usr/bin/env python3
import json
import argparse
import pathlib
import subprocess
from collections import deque
from csv import DictWriter
import math

def run_tests(image, num_trials, start, end, resolution, outfile, retries):
    for i in range(retries):
        print([str(image.absolute()), str(num_trials), str(start), str(end), str(resolution), outfile])
        res = subprocess.run([str(image.absolute()), str(num_trials), str(start), str(end), str(resolution), outfile])
        if res.returncode == 0:
            with open(outfile, 'r') as f:
                return json.load(f)
    return None

def run(image, outdir, num_trials, start, end, resolution, by, retries):
    data = deque()
    fails = deque()
    num_chunks = math.ceil((resolution+end-start)/(resolution*by))
    for i in range(num_chunks):
        chunk_start=start+i*resolution*by
        chunk_end=min(chunk_start+resolution*by, end+resolution)
        name = "out_{}_{}.json".format(chunk_start, chunk_end)
        res = run_tests(image, num_trials, chunk_start, chunk_end, resolution, outdir/name, retries)
        if res is not None:
            data.extend(res)
        else:
            print("Failed to test block {}-{}".format(chunk_start, chunk_end))
            fails.append((chunk_start, chunk_end))
    return (list(data), list(fails))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--start', '-s', help='The packet size to start at', type=int, default=0)
    parser.add_argument('--end', '-e', help='The packet size to end at', type=int, default=5000)
    parser.add_argument('--resolution', help='The packet size resolution to test at', type=int, default=10)
    parser.add_argument('--by', help='The number of different packet sizes to check in one execution of the image', type=int, default=5)
    parser.add_argument('--num-trials', help='The number of trials for each packet size', type=int, default=5)
    parser.add_argument('--outdir', help='The directory to output to', type=pathlib.Path, required=True)
    parser.add_argument('--outfile', help='The file to output to', type=pathlib.Path, required=True)
    parser.add_argument('--image', '-i', help='The image to run to generate packets', type=pathlib.Path, required=True)
    parser.add_argument('--retries', help='The number of times to retry one block of tests', type=int, default=5)
    args = parser.parse_args()
    data,fails = run(args.image, args.outdir, args.num_trials, args.start, args.end, args.resolution, args.by, args.retries)
    with args.outfile.open('w', encoding='UTF-8', newline='') as f:
        writer = DictWriter(f, fieldnames=['kind', 'size', 'res', 'time'])
        writer.writeheader()
        for entry in data:
            writer.writerow(entry)
    for fail in fails:
        print("Failed block: {}-{}".format(fail[0], fail[1]))
        
    
