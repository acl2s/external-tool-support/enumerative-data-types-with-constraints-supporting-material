(include-book "interface/acl2s-utils/top")
(include-book "wifi-model/wifi-model")
(acl2s-defaults :set num-witnesses 0)
:q
(load "try-load-quicklisp.lsp")

(defpackage :z3-wifi-model
  (:use :cl :acl2s))

(in-package :z3-wifi-model)

(import 'acl2s-interface::(itest?-query acl2s-query acl2s-compute acl2s-event))

(acl2s-interface-internal::capture-output-on)

(defun generate-frame-with-size (size)
  (let* ((res (acl2s-event `acl2s::(test? (=> (ManagementProbeRequestFrameBodyTypep msg) (!= (ManagementProbeRequestSize msg) ,z3-wifi-model::size)))))
         (captured-output (acl2s-interface-internal::get-captured-output)))
    (cond ((car res) :sat)
          ((search "Test? proved the conjecture" captured-output) :unsat)
          (t :unknown))))

(ql:quickload :cl-json)
(defun acl2::main (argv)
  (if (not (equal (length argv) 6))
      (format t "Usage: ~a <num-trials> <start> <end> <resolution> <output>" (car argv))
    (let ((n-trials (parse-integer (second argv)))
          (start-size (parse-integer (third argv)))
          (end-size (parse-integer (fourth argv)))
          (resolution (parse-integer (fifth argv)))
          (output-file (sixth argv)))
      (handler-case
          (let ((data
                 (loop for i from start-size below end-size by resolution
                       do (print i)
                       append (loop for j below n-trials
                                    collect (let* ((start-time (get-internal-real-time))
                                                   (res (generate-frame-with-size i))
                                                   (end-time (get-internal-real-time)))
                                              (list i (if (symbolp res) res :sat) (- end-time start-time)))))))
            (with-open-file (stream output-file :if-exists :supersede :direction :output :if-does-not-exist :create)
              (json:use-explicit-encoder)
              (json:encode-json
               (cons :list (loop for (size res time) in data
                                 collect `(:object
                                           (:kind . "acl2s")
                                           (:size . ,size)
                                           (:res . ,res)
                                           (:time . ,(/ time internal-time-units-per-second)))))
               stream)))
        (error (e) (format t "Error: ~a" e)
               (sb-ext:exit :code 1)))
      (sb-ext:exit :code 0))))


(acl2s::save-exec "acl2s-only" nil
                  :init-forms '((acl2::value :q))
                  :toplevel-args "--eval '(acl2::main sb-ext:*posix-argv*)' --disable-debugger")
