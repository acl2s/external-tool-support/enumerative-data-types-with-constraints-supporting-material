#!/bin/bash

mkdir -p output/output-acl2s-only output/output-z3-only output/output-z3-with-acl2s
python3 run-tests.py -s 0 -e 5000 --resolution 10 --by 5 --outdir output/output-acl2s-only/ --outfile output/acl2s-et.csv -i ./acl2s-only  --num-trials 5
python3 run-tests.py -s 0 -e 5000 --resolution 10 --by 5 --outdir output/output-z3-only/ --outfile output/z3.csv -i ./z3-only  --num-trials 5
python3 run-tests.py -s 0 -e 5000 --resolution 10 --by 5 --outdir output/output-z3-with-acl2s/ --outfile output/acl2s-etc.csv -i ./z3-with-acl2s  --num-trials 5
