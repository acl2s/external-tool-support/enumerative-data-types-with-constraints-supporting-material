(include-book "interface/acl2s-utils/top")
(include-book "wifi-model/wifi-model")
:q
(load "try-load-quicklisp.lsp")
(pushnew (truename "./lisp-z3/") ql:*local-project-directories*)
(ql:register-local-projects)
(ql:quickload :lisp-z3)

(defpackage :z3-wifi-model
  (:use :cl :z3 :acl2s))

(in-package :z3-wifi-model)

(import 'acl2s-interface::(itest?-query acl2s-query acl2s-compute acl2s-event))
(import 'acl2::(b* mv))

(solver-init)

(defun in-range (name lo hi)
  `(and (>= ,name ,lo) (<= ,name ,hi)))

;; !!!!!!!!!!!!!!!!!!!!!!!
;; ! NOTE ON SIZE VS LEN !
;; !!!!!!!!!!!!!!!!!!!!!!!
;; In the below code, we talk about SIZE and LENGTH. They are *not* interchangable in this context.
;; LENGTH refers to the length of the body of a frame element - i.e. the length of the array of bytes that constitutes its body.
;; SIZE refers to the size of a frame element - so the number of bytes used by its header field(s) + the number of bytes used by its body.
;; LENGTH is used internally to generate frame elements, whereas SIZE is the value that the user will constrain.

;; Process a list of element descriptions (name size-stmt constraints)
;; name is the name of a variable that corresponds to the LENGTH of a given frame element
;; size-stmt should be something that produces an s-expression corresponding to the element size of the given element when evaluated in an environment where l = name
;; constraints should be something that evaluates to a single s-expression corresponding to any restrictions on the value of the LENGTH (not SIZE) of the element
(defun process-element-descriptions (elt-descs)
  (loop for (name size-stmt constraints) in elt-descs
        append (list name :int) into var-decls
        collect `(and (>= ,name 0) ,(eval `(let ((l ',name)) ,constraints))) into var-constraints
        collect (eval `(let ((l ',name)) ,size-stmt)) into size-stmts
        finally (return (values size-stmts var-decls var-constraints))))


;; Given a set of element descriptions (see make-length-ast) and a
;; size constraint, assert in Z3 that the size constraint is satisfied
;; along with any other constraints on element lengths.
;;
;; size-constraint should be something that evalutes to an
;; s-expression corresponding to a constraint. It is evaluated in an
;; environment where size = a s-expression corresponding to the SIZE
;; (not length) of the entire frame.
(defun assert-frame-size-constraint (elt-descrs size-constraint)
  (multiple-value-bind (size-stmts var-decls var-constraints)
      (process-element-descriptions elt-descrs)
    (z3-assert-fn var-decls
                  `(and ,@var-constraints ,(funcall size-constraint (append '(+ 32) size-stmts))))))

(defmacro with-let-bindings (bindings &body body)
  `(let ,bindings ,@body))

;; Utility macro that executes the body in an environment where the Z3
;; model's variables are bound.
(defmacro with-check-sat (&body body)
  (let ((tmp-var (gensym)))
    `(let ((,tmp-var (check-sat)))
       (if (symbolp ,tmp-var)
           ,tmp-var
           (eval `(with-let-bindings ,,tmp-var ,@',body))))))

;; See comment in generate-frame-body below for more information as to
;; why we have this function.
(defun generate-supportedrateselementtype ()
  (loop for i below 1000
        for res = (acl2s-compute `(b* (((mv val ?seed) (acl2s::NTH-SUPPORTEDRATESELEMENTTYPE/ACC 0 ,(random (expt 2 31)))))
                                    val))
        when (and (not (car res)) (acl2s::supportedrateselementtypep (second res)))
        return (second res)))

;; This code expects to be run in a context where Z3 has had the
;; appropriate assertions added to it regarding the sizes of elements.
;; It does the following:
;; - runs check-sat to get an assignment from Z3 with field lengths,
;;   and creates a let binding for every variable in the returned
;;   model
;; - if check-sat returned a satisfying assignment, generates an ACL2s
;;   query that for each element:
;;   - uses the length value provided by Z3 to call the
;;     element's enumerator and generate an eleemnt body of the
;;     appropriate type
;;   - uses the defdata constructor for the element type to
;;     construct it from its constituent pieces
;; - finally the ACL2s query calls the constructor for the frame type
;;   itself, using the elements generated.
(defun generate-frame-body ()
  (with-check-sat
   (let ((res (acl2s-compute
               `(acl2::b*
                 ((seed ,(random (expt 2 31)))
                  ;; Note that the ACL2s model does not consider
                  ;; either the SSID or SupportedRate elements to
                  ;; count towards the size of a frame. Therefore, we
                  ;; pass 0 into both instead of a length variable.
                  ((mv ssd seed) (acl2s::NTH-SSIDELEMENTTYPE/ACC 0 seed))
                  ;; This is a hack to get around the fact that the
                  ;; enumerator for SupportedRatesElementType only
                  ;; generates a valid element of the type about 50%
                  ;; of the time.
                  ;; Note that this results in a noticible reduction
                  ;; in performance, since ACL2s is going to be queried
                  ;; at least one more time, which is expensive.
                  (sre ',(generate-supportedrateselementtype))
                  ;; The body of this frame is not a list, so the
                  ;; first argument to the enumerator doesn't do
                  ;; anything.
                  ((mv dss seed) (acl2s::NTH-DSSSPARAMETERSETELEMENTTYPE/ACC 0 seed))
                  ((mv ret-body seed) (acl2s::nth-byte255-increasing/acc ,lRequest seed))
                  (ret (acl2s::RequestElementType 10 ret-body))
                  ((mv esr-body seed) (acl2s::nth-supportedrate255/acc ,lExtendedSupportedRates seed))
                  (esr (acl2s::ExtendedSupportedRatesElementType 50 esr-body))
                  ((mv htc-body seed) (acl2s::nth-byte26/acc ,lHTCapabilities seed))
                  (htc ;; TODO this is always length 26...
                   (acl2s::HTCapabilitiesElementType 45 htc-body))
                  ((mv eca-body seed) (acl2s::nth-byte10/acc ,lExtendedCapabilities seed))
                  (eca
                   (acl2s::ExtendedCapabilitiesElementType 127 eca-body))
                  ((mv iwk-body seed) (acl2s::nth-byte9/acc ,lInterworking seed))
                  (iwk
                   (acl2s::InterworkingElementType 107 iwk-body))
                  ((mv mid-body seed) (acl2s::nth-byte32/acc ,lMeshID seed))
                  (mid (acl2s::MeshIDElementType 114 mid-body))
                  ((mv dmg-body seed) (acl2s::nth-byte22/acc ,lDMGCapabilities seed))
                  (dmg ;; TODO this is always length 22...
                   (acl2s::DMGCapabilitiesElementType 148 dmg-body))
                  ((mv vht-body seed) (acl2s::nth-byte12/acc ,lVHTCapabilities seed))
                  (vht ;; TODO this is always length 12...
                   (acl2s::VHTCapabilitiesElementType 26 vht-body))
                  ((mv est-body seed) (acl2s::nth-byte3-12/acc ,lEstimatedServiceParameters seed))
                  (est (acl2s::EstimatedServiceParametersElementType 255 11 est-body))
                  ((mv ere-body seed) (acl2s::nth-byte255/acc ,lExtendedRequest seed))
                  (ere (acl2s::ExtendedRequestElementType 255 10 ere-body))
                  ((mv soc-body seed) (acl2s::nth-byte1-255/acc ,lSupportedOperatingClasses seed))
                  (soc (acl2s::SupportedOperatingClassesElementType 59 soc-body))
                  ((mv bss-body seed) (acl2s::nth-byte1-255/acc ,lBSS2040Coexistence seed))
                  (bss (acl2s::BSS2040CoexistenceElementType 72 bss-body))
                  ((mv ssl-body seed) (acl2s::nth-byte255/acc ,lSSIDList seed))
                  (ssl (acl2s::SSIDListElementType 84 ssl-body))
                  ((mv chu-body seed) (acl2s::nth-byte255-odd/acc ,lChannelUsage seed))
                  (chu (acl2s::ChannelUsageElementType 97 chu-body))
                  ((mv mbn-body seed) (acl2s::nth-byte22-255/acc ,lMultiBand seed))
                  (mbn (acl2s::MultiBandElementType 158 mbn-body))
                  ((mv mms-body seed) (acl2s::nth-byte255-7-6/acc ,lMultipleMacSublayers seed))
                  (mms (acl2s::MultipleMacSublayersElementType 170 mms-body))
                  ((mv vsp-body ?seed) (acl2s::nth-byte3-255/acc ,lVenderSpecific seed))
                  (vsp (acl2s::VenderSpecificElementType 221 vsp-body))
                  (framebody
                   (acl2s::ManagementProbeRequestFrameBodyType
                    ssd
                    sre
                    dss
                    ret
                    htc
                    esr
                    soc
                    bss
                    ssl
                    chu
                    iwk
                    mid
                    eca
                    dmg
                    mbn
                    mms
                    vht
                    vsp
                    ere
                    est)))
                 framebody))))
     (unless (car res)
       (second res)))))

(defun test-size (sz)
  (solver-push)
  (assert-frame-size-constraint
   '((lRequest `(+ 2 ,l) (in-range l 0 255))
     (lExtendedSupportedRates `(+ 2 ,l) (in-range l 1 255))
     ;;(lDSSSParameterSet '3 t) ;; we don't have any additional constraints here, we don't really care about this size.
     (lHTCapabilities `(+ 2 ,l) `(= ,l 26))
     (lExtendedCapabilities `(+ 2 ,l) (in-range l 1 10))
     (lInterworking `(+ 2 ,l) (in-range l 1 9))
     (lMeshID `(+ 2 ,l) (in-range l 0 32))
     (lDMGCapabilities `(+ 2 ,l) `(= ,l 22))
     (lVHTCapabilities `(+ 2 ,l) `(= ,l 12))
     (lEstimatedServiceParameters `(+ 3 ,l) `(or (= ,l 3) (= ,l 6) (= ,l 9) (= ,l 12)))
     (lExtendedRequest `(+ 3 ,l) (in-range l 0 255))
     (lSupportedOperatingClasses `(+ 2 ,l) (in-range l 1 255))
     (lBSS2040Coexistence `(+ 2 ,l) (in-range l 1 255))
     (lSSIDList `(+ 2 ,l) (in-range l 0 255))
     (lChannelUsage `(+ 2 ,l) (cons 'or (loop for i from 1 to 255 by 2 collect `(= ,l ,i))))
     (lMultiBand `(+ 2 ,l) (cons 'or (loop for i from 22 to 255 by 2 collect `(= ,l ,i))))
     (lMultipleMacSublayers `(+ 2 ,l) (cons 'or (loop for i from 7 to 255 by 6 collect `(= ,l ,i))))
     (lVenderSpecific `(+ 2 ,l) (in-range l 3 255)))
   (lambda (size) `(= (+ ,size 3) ,sz))) ;; +3 because we commented out lDSSSParameterSet
  (let ((res (check-sat)))
    (prog1
        (if (symbolp res)
            res
          (generate-frame-body))
      (solver-pop))))

(ql:quickload :cl-json)
(defun acl2::main (argv)
  (if (not (equal (length argv) 6))
      (format t "Usage: ~a <num-trials> <start> <end> <resolution> <output>" (car argv))
    (let ((n-trials (parse-integer (second argv)))
          (start-size (parse-integer (third argv)))
          (end-size (parse-integer (fourth argv)))
          (resolution (parse-integer (fifth argv)))
          (output-file (sixth argv)))
      (acl2s-interface::quiet-mode-on)
      (solver-init)
      (handler-case
          (let ((data
                 (loop for i from start-size below end-size by resolution
                       do (print i)
                       append (loop for j below n-trials
                                    collect (let* ((start-time (get-internal-real-time))
                                                   (res (test-size i))
                                                   (end-time (get-internal-real-time)))
                                              ;; Double-check that we generated something of the right size
                                              ;; This will also fail if the generated value isn't of the right type.
                                              (when (and res (not (symbolp res)))
                                                (assert (equal (acl2s-compute `(acl2s::managementproberequestsize ',res)) (list nil i))))
                                              (list i (cond ((symbolp res) res)
                                                            ((not res) :fail)
                                                            (t :sat))
                                                    (- end-time start-time)))))))
            (with-open-file (stream output-file :if-exists :supersede :direction :output :if-does-not-exist :create)
              (json:use-explicit-encoder)
              (json:encode-json
               (cons :list (loop for (size res time) in data
                                 collect `(:object
                                           (:kind . "z3-with-acl2s")
                                           (:size . ,size)
                                           (:res . ,res)
                                           (:time . ,(/ time internal-time-units-per-second)))))
               stream)))
        (error (e) (format t "Error: ~a" e)
               (sb-ext:exit :code 1)))
      (sb-ext:exit :code 0))))


(acl2s::save-exec "z3-with-acl2s" nil
                  :init-forms '((acl2::value :q))
                  :toplevel-args "--eval '(acl2::main sb-ext:*posix-argv*)' --disable-debugger")
