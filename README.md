This directory contains supporting materials for the paper
"Enumerative Data Types with Constraints" submitted to FMCAD 2022.


The data used to generate the plot shown in the paper is available in
CSV form in the `data` directory.


The model performance experiments can be rerun by building and running
the Docker image contained in the `experiment` directory. To build the Docker
image, run `make docker-image` (you will need both `make` and Docker
installed). To run the resulting image, run `make run-docker-image`
afterwards. Results will be placed in the `output` directory - the
three `.csv` files at the top level of the `output` directory are the
ones in the `data` directory.


The Lustre model is available in the root directory in
`lustre-model.lus`.
